FROM alpine:3.7

WORKDIR /app

ADD package.json /app/package.json

RUN true \
    && apk --no-cache --update add nodejs ruby shadow \
    && rm -rf /var/cache/apk/* \
    && npm install \
    && adduser -S -u 2000 -s /bin/sh -h /app appuser \
    && chown -R appuser /app

ADD generate_config.rb /generate_config.rb
ADD webpack.config.js.erb /webpack.config.js.erb
ADD init.sh /init.sh

CMD ["/init.sh"]
