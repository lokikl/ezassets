#!/usr/bin/ruby

require 'yaml'
require 'erb'

if ENV['CONFIG'].to_s == ''
  raise "ENV['CONFIG'] is expected but missing"
end
$config = YAML.load(ENV['CONFIG'])

erb_template = File.read('/webpack.config.js.erb')
renderer = ERB.new(erb_template, 0, '-')
File.write('/app/webpack.config.js', renderer.result)
