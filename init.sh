#!/usr/bin/env sh
set -e

echo "Generate bind config from yaml file"
/generate_config.rb

if [[ "$WEBPACK_UID" != "" ]]; then
  echo "Set uid for appuser to ${WEBPACK_UID}"
  usermod -u ${WEBPACK_UID} appuser
fi

su -c "npm run watch" appuser
